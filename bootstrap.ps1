$programs = @(
    @{
        "url" = "https://download.mozilla.org/?product=firefox-latest-ssl&os=win64&lang=en-US"
        "local" = "firefox.exe"
        "arguments" = "-ms"
        "final" = "C:\Program Files\Mozilla Firefox\firefox.exe"
    }
    @{
        "url" = "https://steamcdn-a.akamaihd.net/client/installer/SteamSetup.exe"
        "local" = "steam.exe"
        "arguments" = "-silent"
        "final" = "C:\Program Files (x86)\Steam\Steam.exe"
    }
    @{
        "url" = "filezilla"
        "local" = "filezilla.exe"
        "arguments" = "/S /user=all"
        "final" = "C:\Program Files\FileZilla FTP Client\filezilla.exe"
    }
    @{
        "url" = "notepad++"
        "local" = "notepad++.exe"
        "arguments" = "/S"
        "final" = "C:\Program Files\Notepad++\notepad++.exe"
    }
    @{ # Installer fails silently when using Silent mode
        "url" = "vlc"
        "local" = "vlc.exe"
        "arguments" = "/L=1033 /S"
        "final" = "C:\Program Files\VideoLan\vlc.exe"
    }
    @{
        "url" = "7zip"
        "local" = "7zip.exe"
        "arguments" = "/S"
        "final" = "C:\Program Files\7-Zip\7zG.exe"
    }
    #@{ # Keepass doesn't work right now since PowerShell can't easily reach TLS 1.2-only websites like Sourceforge
        #"url" = "keepass"
        #"local" = "keepass.exe"
        #"arguments" = "/S"
        #"final" = "C:\Program Files\Keepass"
    #}
)

$find_urls = @{
    "filezilla" = @{
        "parse_url" = "https://filezilla-project.org/download.php?show_all=1"
        "regexp" = "https://dl2.cdn.filezilla-project.org/client/FileZilla_[0-9\.]+_win64-setup.exe"
    }
    "notepad++" = @{
        "parse_url" = "https://notepad-plus-plus.org/download/"
        "regexp" = "https://notepad-plus-plus.org/repository/[0-9]+.x/[0-9\.]+/npp.[0-9\.]+.Installer.x64.exe"
    }
    "vlc" = @{
        "parse_url" = "https://www.videolan.org/vlc/download-windows.html"
        "regexp" = "//get.videolan.org/vlc/[0-9\.]+/win64/vlc-[0-9\.]+-win64.exe"
    }
    "7zip" = @{
        "parse_url" = "https://www.7-zip.org/"
        "regexp" = "a/7z[0-9]+-x64.exe"
    }
    "keepass" = @{
        "parse_url" = "https://keepass.info/download.html"
        "regexp" = "https://sourceforge.net/projects/keepass/files/KeePass%202.x/2.[0-9]+/KeePass-2.[0-9]+-Setup.exe/download"
    }
}

$registry = @(
    @{
        "path" = "HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server"
        "name" = "fDenyTSConnections"
        "value" = "0"
        "type" = "DWORD"
    }
    @{
        "path" = "HKLM:\SOFTWARE\Policies\Microsoft\Windows\System"
        "name" = "NoLocalPasswordResetQuestions"
        "value" = "1"
        "type" = "DWORD"
    }
)

$winstoreclutter = @(
    "Microsoft.People"
    "Microsoft.Office.OneNote"
    "Microsoft.ZuneVideo"
    "Microsoft.MicrosoftSolitaireCollection"
    "Microsoft.WindowsMaps"
    "Microsoft.ZuneMusic"
    "Microsoft.OneConnect"
    "Microsoft.Getstarted"
    "Microsoft.SkypeApp"
    "Microsoft.MicrosoftOfficeHub"
    "Microsoft.WindowsCamera"
    "microsoft.windowscommunicationsapps"
    "Microsoft.3DBuilder"
    "Microsoft.MSPaint"
    "Microsoft.MicrosoftStickyNotes"
    "Microsoft.WindowsAlarms"
)

# Register TLSv1.2

try {
    if ([Net.ServicePointManager]::SecurityProtocol -notcontains 'Tls12') {
        [Net.ServicePointManager]::SecurityProtocol += [Net.SecurityProtocolType]::Tls12
    }
}

function Find-Latest-Program-URL {
    [cmdletbinding()]
    Param (
        [string]$Program
    )
    Process {
        $setup_url = ""
        Write-Host "Find URL for latest $Program setup"
        if($find_urls.ContainsKey($Program)) {
            $parse_url = $find_urls.Item($Program).Item("parse_url")
            $regexp = $find_urls.Item($Program).Item("regexp")
            
            $result = Invoke-WebRequest $parse_url -UseBasicParsing
            foreach($link in $result.Links) {
                if($link.href -match $regexp) {
                    $setup_url = $link.href
                    break
                }
            }
        }
        else {
            Throw New-Object System.FormatException "Unknown program $Program in URL finding"
        }
        if($setup_url.StartsWith('//')) {
            $setup_url = "https:${setup_url}"
        }
        elseif(!($setup_url -match '^(https?|ftp)://')) {
            $setup_url = "$parse_url/$setup_url"
        }
        Write-Output $setup_url
    }
}

function Install-Remote-Program {
    [cmdletbinding()]
    Param (
        [string]$SetupName,
        [string]$Remote,
        [string]$Arguments,
        [string]$Final
    )
    Process {
        if(!(Test-Path $Final)) {
            $local = Join-Path -Path ${env:TEMP} -ChildPath $SetupName
            if(!($Remote -match '^(https?|ftp)://')) {
                $setup_url = Find-Latest-Program-URL -Program $Remote
            }
            else {
                $setup_url = $Remote
            }
            Write-Host "Fetching program $setup_url"
            # Handle redirections
            $request = [system.Net.HttpWebRequest]::Create($setup_url)
            $request.Method = "HEAD"
            $response = $request.getresponse()
            if($response.ResponseUri -ne $setup_url) {
                $setup_url = $response.ResponseUri
            }
            Start-BitsTransfer -Source $setup_url -Destination $local

            Write-Host "Installing program $local"
            Start-Process -FilePath $local -ArgumentList $Arguments -NoNewWindow -Wait
            Write-Host "Remove program setup $local"
            #Remove-Item $local
        }
        else {
            Write-Host "$Final is already installed"
        }
    }
}

function Set-Registry-Key {
    [cmdletbinding()]
    Param (
        [string]$Path,
        [string]$Name,
        [string]$Value,
        [string]$Type
    )
    Process {
        if(!(Test-Path $Path)) {
            Write-Host "Creating registry path $Path"
            New-Item -Path $Path -Force
        }
        $current_reg = Get-ItemProperty -Path $Path -Name $Name
        if($current_reg.$Name -ne $Value) {
            Write-Host "Setting registry key $Path $Name to $Type $Value"
            New-ItemProperty -Path $Path -Name $Name -Value $Value -PropertyType $Type -Force
        }
        else {
            Write-Host "Registry key $Path $Name already set to $Value"
        }
    }
}

# Remove some installed Modern Apps
Get-AppxPackage -AllUsers | foreach {
    $name = $_.Name
    if($winstoreclutter.Contains($name)) {
        Write-Host "Remove Modern App $name"
        Get-AppxPackage -Name $name -AllUsers | Remove-AppxPackage -AllUsers
    }
}

# Do the same for future new users
Get-AppxProvisionedPackage -Online | foreach {
    $name = $_.DisplayName
    if($winstoreclutter.Contains($name)) {
        Write-Host "Remove provisioned Modern App $name"
        Remove-AppxProvisionedPackage -PackageName $_.PackageName -Online
    }
}

# Install some programs
foreach ($p in $programs) {
    Install-Remote-Program -Remote $p.Item("url") -SetupName $p.Item("local") `
        -Arguments $p.Item("arguments") -Final $p.Item("final")
}

# Set some registry keys
foreach ($r in $registry) {
    Set-Registry-Key -Path $r.Item("path") -Name $r.Item("name") `
        -Value $r.Item("value") -Type $r.Item("type")
}
